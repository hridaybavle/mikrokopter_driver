#include "ros/ros.h"
#include "std_msgs/String.h"


#include <mikrokopter_driver/OktoCommand.h>
#include <mikrokopter_driver/OktoSensorData.h>

#include <mikrokopter/kopter.h>
#include <mikrokopter/io/console.h>
#include <mikrokopter/io/serial.h>
#include <mikrokopter/common/time.h>


mikrokopter::protocol::ExternControl control;


void Commandcallback(const mikrokopter_driver::OktoCommand::ConstPtr& msg)
{

   control.config = 1;
   control.pitch = msg->nick;
   control.roll = msg->roll;
   control.yaw = msg->dyaw;
   control.height = msg->height;
   control.gas = msg->gas;
   return;

}

int main(int argc, char** argv)
{
  try
  {

   ros::init(argc, argv,"mikrokopter_driver_sub_node");
   ros::NodeHandle n;
   ros::Publisher  chatter_pub = n.advertise<mikrokopter_driver::OktoSensorData>("Okto_sensor_data", 1,true);
   ros::Subscriber chatter_sub = n.subscribe("Okto_command",1,Commandcallback);
   ros::Rate loop_rate(40);


    std::string port;                                                   //"/dev/ttyUSB0"
    n.getParam("mikrokopter_node/comm_port",port);

    if (argc > 1)
    port = argv[1];
    std::cout << "Opening MikroKopter communication on port " << port << std::endl;
    mikrokopter::io::IO::Ptr comm(new mikrokopter::io::Serial(port));
    //mikrokopter::io::IO::Ptr comm(new mikrokopter::io::Console());
    mikrokopter::Kopter kopter(comm);

    // set debug level to see all messages sent and received
    // comm->DEBUG_LEVEL = 1;
    
    //kopter.connectNaviCtrl();
    kopter.connectFlightCtrl();
    //kopter.connectMK3MAG();

    /*
      left-handed frame, x front*/
      
    const int debug_request_interval = 10;

    while(1)
    {
     ros::spinOnce();
     
     kopter.sendExternalControl(control);
     
     kopter.requestDebugData(debug_request_interval);
     mikrokopter::protocol::FlightControlDebugData debugout = kopter.getDebugData();
     mikrokopter_driver::OktoSensorData sensorData;

    
     sensorData.header.stamp           = ros::Time::now();
     sensorData.integrated_nick        = debugout.data[0];
     sensorData.integrated_roll        = debugout.data[1];
     sensorData.mean_acceleration_nick = debugout.data[2];
     sensorData.mean_acceleration_roll = debugout.data[3];
     sensorData.yaw                    = debugout.data[4];
     sensorData.height                 = debugout.data[5];
     sensorData.acceleration_up        = debugout.data[6];
     sensorData.gas                    = debugout.data[7];
     sensorData.compass                = debugout.data[8];
     sensorData.battery_voltage        = debugout.data[9];
     sensorData.sensor_ok              = debugout.data[10];
     sensorData.backup_compass_deg     = debugout.data[11];
     //sensorData.motor_set_points     = debugout.data[12];
     sensorData.stick_nick             = debugout.data[13];
     sensorData.stick_roll             = debugout.data[14];
     sensorData.stick_gier             = debugout.data[15];
     sensorData.stick_gas              = debugout.data[16];
     sensorData.servo_nick             = debugout.data[17];
     sensorData.hover_gas              = debugout.data[18];
     sensorData.current                = debugout.data[19];
     sensorData.capacity_used          = debugout.data[20];
     sensorData.height_set_point       = debugout.data[21];
     sensorData.external_control       = debugout.data[22];
     //sensorData.another_unused       = debugout.data[23];
     sensorData.compass_set_point      = debugout.data[24];
     sensorData.i2c_error              = debugout.data[25];
     sensorData.capacity_min_of_max_pwm = debugout.data[26];
     sensorData.gps_nick                = debugout.data[27];
     sensorData.gps_roll                = debugout.data[28];


     chatter_pub.publish(sensorData);

     loop_rate.sleep();

    }

  }
  catch( boost::system::system_error& e)
  {
      std::cerr << "ERROR: " << e.what() << std::endl;
  }
  catch(...)
  {
      std::cerr << "Caught unhandled exception!" << std::endl;
  }
  
      return 0;
}

